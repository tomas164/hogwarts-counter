# hogwarts-counter

- FastAPI
- autentifikace: [basicAUTH](https://fastapi.tiangolo.com/advanced/security/http-basic-auth/)
- server: [uvicron](https://www.uvicorn.org/#introduction)

## Star instructions
```
poetry shell # to start the poetry venv
uvicorn main:app --reload # start server
```
