# main.py

import json
import requests
import random
from datetime import datetime

from fastapi import FastAPI, Request
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles


templates = Jinja2Templates(directory="templates")
LOGS_LINK = "https://script.googleusercontent.com/macros/echo?user_content_key=kBEFAinRebfmRMokxdhyDMLcFxhtpwSHf8ht1_A1KXjNjyw4DOPQynLCrkHik7Jadj3oLRVvGKIATUVuNc3ZAnEQKFqYVVGmm5_BxDlH2jW0nuo2oDemN9CCS2h10ox_1xSncGQajx_ryfhECjZEnMorA0nNK16qohzgVEEAoSIht6x7m7uoUmsJTQXpyx4zKTvv4b2r9c1RTVFFVmRtu5J-GygUlTshOyUzUxV-ap68cv7vWJ9jUQ&lib=Mejmu3T7UFkPkZIqk6osDMhWMXI2VoG64"
PROPHET_LINK = "https://script.googleusercontent.com/macros/echo?user_content_key=iyQElmESoOmGpT9Ejmv6daeq1OSQ9hvrqZHU3A2g_8Bnba0Jjutgpuq-m-u7H_t1CFdheVB0sJ2g7siZgFjbuBp-Wsn4lJcWm5_BxDlH2jW0nuo2oDemN9CCS2h10ox_1xSncGQajx_ryfhECjZEnLSdSVGPraV6Ul1yONzgtfAYPMrVWkVp66Y_9Nrm-sEdKeCVJxN2x5Z80CUeQjJiSEd_DoqLMzjCm04MvOYdX--8cv7vWJ9jUQ&lib=Mejmu3T7UFkPkZIqk6osDMhWMXI2VoG64"
CHART_LINK = "https://script.google.com/macros/s/AKfycbwMAS0IjjnQo6SgheMUqJ-rDj4YErkAW1UAqKC7p7eAoYJKV5VUwI8-8KguUd-S618/exec"
GROUP_COUNT = 4
COLORS = [
    "#23D0FF",
    "#704AFF",
    "#FF8417",
    "#54E45B",
]

app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")


def parse_time(date_str: str):
    # 2024-05-06T16:22:00.376Z
    TIME_FORMAT = "%Y-%m-%dT%H:%M:%S"
    # HACK: remove last 5 characters instead of figuring out hot to properly parse time
    return datetime.strptime(date_str[:-5], TIME_FORMAT)


def edit_field(data, field, edit_func):
    # Remove all items of list `data` which do not include field `field`
    data = list(filter(lambda item: field in item.keys(), data))
    # Edit field
    for item in data:
        item[field] = edit_func(item[field])
    return data


def get_points_sum():
    sum_by_group = json.loads(requests.get(CHART_LINK).text)
    points = {"labels": [], "data": [], "colors": [], "oddil": []}
    for group in sum_by_group:
        if is_group_correct(group):
            points["labels"].append(group["KOLEJ"])
            points["data"].append(group["BODY"])
            points["colors"].append(COLORS[group["ODDIL"] - 1])
            points["oddil"].append(group["ODDIL"])
    # sorting by points count
    points["oddil"] = (
        list(t)
        for t in zip(*sorted(zip(points["oddil"])))
    )
    return points


def is_group_correct(log):
    return "ODDIL" in log.keys() and 0 < log["ODDIL"] < GROUP_COUNT + 1


def pick_random_active_article():
    articles = json.loads(requests.get(PROPHET_LINK).text)
    articles = list(
        filter(
            lambda article: "Aktivni" in article.keys() and article["Aktivni"] == "A",
            articles,
        )
    )
    if len(articles) <= 0:
        return {}
    return articles[random.randint(0, len(articles) - 1)]


def get_logs_data():
    response = json.loads(requests.get(LOGS_LINK).text)
    return edit_field(response, "CAS", parse_time)


@app.get("/")
async def root(request: Request):
    return templates.TemplateResponse(
        "index.jinja.html",  # name of the template in /templates folder
        {
            "request": request,  # needed to render the page
        },
        # TODO: Pass list of available dates
    )


@app.get("/logs")
async def get_logs(request: Request):
    logs = get_logs_data()
    chartData = get_points_sum()
    return templates.TemplateResponse(
        "logList.jinja.html", {"request": request, "logs": logs, "points": chartData}
    )


@app.get("/days/{date}")
async def get_logs_by_day(request: Request, date: str):
    logs = get_logs_data()
    current_date = datetime.strptime(date, "%Y-%m-%d")
    # Filter logs by date
    logs = list(filter(lambda log: log["CAS"].date() == current_date.date(), logs))
    return templates.TemplateResponse(
        "logList.jinja.html", {"request": request, "logs": logs}
    )


@app.get("/logs/{group}")
async def get_logs_by_group(request: Request, group: int):
    logs = get_logs_data()
    # Filter logs by group
    logs = list(
        filter(lambda log: "ODDIL" in log.keys() and log["ODDIL"] == group, logs)
    )
    return templates.TemplateResponse(
        "logList.jinja.html", {"request": request, "logs": logs}
    )


@app.get("/prophet")
async def get_daily_prophet(request: Request):
    # Different types of img links
    #             "imgURL4": "https://drive.google.com/thumbnail?id=1sr2nv9ZqZRVun6wz0aETtw3zU0xnF4Bn",  # works but it is just a small thumbnail
    #             "imgURL5": "https://drive.google.com/thumbnail?id=1wZ44hnjd-tIjpu6a1MP1FnISTm52Nwaw",
    #             "imgURL2": "https://lh3.googleusercontent.com/d/1wZ44hnjd-tIjpu6a1MP1FnISTm52Nwaw=s2200",  # this works, I am not sure if it is permanent though
    #             "imgURL3": "https://drive.google.com/uc?id=1sr2nv9ZqZRVun6wz0aETtw3zU0xnF4Bn",
    #             "imgURL": "https://lh3.googleusercontent.com/d/1sr2nv9ZqZRVun6wz0aETtw3zU0xnF4Bn=s2200",  # this works, I am not sure if it is permanent though
    response = json.loads(requests.get(PROPHET_LINK).text)
    return templates.TemplateResponse(
        "dailyProphetList.jinja.html", {"request": request, "prophet": response}
    )


@app.get("/offline/prophet")
async def offline_prophet(request: Request):
    return templates.TemplateResponse(
        "dailyProphetList.jinja.html",
        {
            "request": request,
            "prophet": [
                {
                    "Aktivni": "A",
                    "Nadpis": "Harry si vozí prdel na hipogryfovi",
                    "Text": "A vůbec se u toho nestydí - líbí se mu to, zlobínkovi!",
                    "Fotka na GDrive": "https://drive.google.com/file/d/1sr2nv9ZqZRVun6wz0aETtw3zU0xnF4Bn/view",
                    "ImgLink": "1sr2nv9ZqZRVun6wz0aETtw3zU0xnF4Bn",
                }
            ],
        },
    )


@app.get("/offline/logs")
async def offline_logs(request: Request):
    logs = [
        {
            "CAS": "2024-05-06T16:22:00.376Z",
            "BODY": 5,
            "PROFESOR": "Moody",
            "KOLEJ": "Prcimor",
            "ODDIL": 1,
            "POZNAMKA": "Nudim se",
        },
        {
            "CAS": "2024-05-06T16:22:00.376Z",
            "BODY": 5,
            "PROFESOR": "Moody",
            "KOLEJ": "Prcimor",
            "ODDIL": 2,
            "POZNAMKA": "Nudim se",
        },
        {
            "CAS": "2024-05-06T16:22:00.376Z",
            "BODY": 5,
            "PROFESOR": "Moody",
            "KOLEJ": "Prcimor",
            "ODDIL": 3,
            "POZNAMKA": "Nudim se",
        },
        {
            "CAS": "2024-05-06T16:22:00.376Z",
            "BODY": 5,
            "PROFESOR": "Moody",
            "KOLEJ": "Prcimor",
            "ODDIL": 4,
            "POZNAMKA": "Nudim se",
        },
    ]
    logs = edit_field(logs, "CAS", parse_time)
    chartData = [
        {
            "colors": [
                "#FF0000",
                "#00FF00",
                "#0000FF",
                "#FF0000",
            ],
            "data": [12, 30, 4, 5],
            "lables": ["label1", "label2", "label3", "label4"],
        }
    ]
    return templates.TemplateResponse(
        "logList.jinja.html", {"request": request, "logs": logs, "points": {}}
    )


@app.get("/live")
async def get_live(request: Request):
    logsData = json.loads(requests.get(LOGS_LINK).text)
    prophetData = pick_random_active_article()
    points = get_points_sum()
    logsData = edit_field(logsData, "CAS", parse_time)
    return templates.TemplateResponse(
        "live.jinja.html",
        {
            "request": request,
            "logs": logsData,
            "prophet": [prophetData],
            "points": points,
        },
    )
